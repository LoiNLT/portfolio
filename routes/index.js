var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('home/index', { title: 'Express' });
});

/* GET detail page. */
router.get('/detail', function(req, res, next) {
  res.render('home/detail', { title: 'Detail' });
});

module.exports = router;
